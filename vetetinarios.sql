﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 3 tablas

DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE b20190528;
USE b20190528;

/*
  Creando la tabla animales
*/
CREATE TABLE animales(
    id int AUTO_INCREMENT,
    nombre varchar(100),
    raza varchar(100),
    fechaNac date,
    PRIMARY KEY(id) -- creando la clave
  );

CREATE TABLE veterinarios(
    cod int AUTO_INCREMENT,
    nombre varchar(100),
    especialidad varchar(100),
    PRIMARY KEY(cod)
  );


CREATE TABLE acuden(
    idanimal int,
    codVet int,
    fecha date,
    PRIMARY KEY(idanimal,codVet,fecha),
    UNIQUE KEY(idanimal),
    CONSTRAINT fkAcudenAnimal FOREIGN KEY (idanimal) 
    REFERENCES animales(id),
    CONSTRAINT fkAcudenVet FOREIGN KEY (codVet)
    REFERENCES veterinarios(cod)
  );

-- Insertar un registro

INSERT INTO animales (nombre, raza, fechaNac)
  VALUES ('jorge','bulldog','2000/02/01'),
         ('ana','caniche','2002/01/01');

-- Listar 
SELECT * FROM animales;